# OpenID Connect 1.0 Client Tool For Developers

Copyright (c) Connect2id Ltd., 2012 - 2021

Relying party (client) software for testing and debugging user authentication 
with [OpenID Connect](https://connect2id.com/learn/openid-connect).


![Screenshot](https://bitbucket.org/connect2id/openid-connect-dev-client/raw/f6e681101fe3049eabf660123e2f28ff2032f03d/screenshot.png "Screenshot")


## License

This software is built with open source under the terms of the Apache 2.0 
licence.


## Usage

To perform OpenID authentication for an end-user the client must be configured
with the details of the OpenID provider and OpenID relying party.

### OpenID provider details

Open the "OpenID provider" tab and set the issuer URL. This is an `https` URL
that uniquely identifies the OpenID provider, e.g. as `https://demo.c2id.com`.
ID tokens issued by the OpenID provider are going to include an `iss` claim set
to this URL.

By clicking on "Endpoints" -> "Query" the client will use the entered issuer 
URL to retrieve from the OpenID provider a [JSON 
document](https://openid.net/specs/openid-connect-discovery-1_0.html#ProviderMetadata) 
listing the provider endpoints and capabilities. This document sits at a 
well-known location composed by appending `.well-known/openid-configuration` to 
the issuer URL.

Alternatively, the "Edit manually" checkbox allows manual entry of the OpenID 
provider endpoints:

* JSON Web Key set -- The URL of the public OpenID provider keys for verifying
  the ID tokens issued by the provider.

* Authorization endpoint -- The OAuth 2.0 authorization endpoint of the OpenID
  provider where the browser is redirected to let the end-user to be 
  authenticated and consent login into the requesting OpenID relying party 
  (this client).
  
* Token endpoint -- The OAuth 2.0 token endpoint where the OpenID relying party
  can retrieve the requested tokens (ID token, access token and / or refresh
  token) after a successful interaction at the authorization endpoint.
  
* UserInfo endpoint -- The OpenID provider endpoint where the OpenID relying
  party can retrieve the consented details about the end-user, requires a valid
  access token.


### OpenID relying party details

Open the "OpenID relying party" tab to enter the details of the OpenID relying 
party obtained when it was registered as a client software with the OpenID 
provider. If you are using the Connect2id server check the [client registration 
guide](https://connect2id.com/products/server/docs/guides/client-registration)
for examples.

* Client ID -- The `client_id` of the OpenID relying party.
  
* Client authentication -- The registered authentication method 
  (`token_endpoint_auth_method`) for the OpenID relying party. For a public 
  client (no credentials) select `none`.
  
* Client secret -- The registered `client_secret`.

* Redirection URI -- A registered `redirect_uris` value for the OpenID relying
  party, to be used as the `redirect_uri` parameter in the OpenID 
  authentication request. This should be an `https` URL.


### Authenticate end-user

Open this tab to perform an OpenID authentication.

* Response type -- The value of the OAuth 2.0 `response_type` authorization 
  request parameter. Should be left at its default setting `code`.
  
* Scope -- The requested scope values for the UserInfo endpoint. The `openid`
  value is always ticked to identify the OAuth 2.0 authorization request as an
  OpenID authentication.
  
* Options -- Click on "Show" to set optional parameters.


By clicking on "Log in with OpenID Connect" the client will compose an 
[OpenID authentication request](https://openid.net/specs/openid-connect-core-1_0.html#AuthRequest)
and send the browser to the OpenID provider authorization endpoint where the
end-user will be asked to authenticate and consent login.

The redirection to the OpenID provider and the display of the results will be
made in a pop-up window.


## Initial OpenID provider and relying party settings

The initial settings in the OpenID provider and relying party tabs are 
populated from the `WEB-INF/client.properties` file.

For the client at [https://demo.c2id.com/oidc-client/](https://demo.c2id.com/oidc-client/)
they look like this:

```ini
# OpenID provider (OP) properties
op.iss = https://demo.c2id.com/c2id
op.jwks_uri = https://demo.c2id.com/c2id/jwks.json
op.authz_uri = https://demo.c2id.com/c2id-login-page-js
op.token_uri = https://demo.c2id.com/c2id/token
op.userinfo_uri = https://demo.c2id.com/c2id/userinfo

# OpenID relying party (RP) properties
rp.client_id = 000123
rp.client_secret = 7wKJNYFaKKg4FxUdi8_R75GGYsiWezvAbcdN1uSumE4
rp.redirect_uri = https://demo.c2id.com/oidc-client/cb
```

## HTTP proxy

To configure an HTTP proxy for the back-channel requests from the client to the
OpenID provider server set the `http.proxyHost` and `http.proxyPort` Java 
system properties.

If the client is deployed in a Apache Tomcat container open 
`tomcat/bin/setenv.sh` to set the proxy settings via the `-D` java command line
option. For example:

```ini
export CATALINA_OPTS="$CATALINA_OPTS -Dhttp.proxyHost=proxy.example.com"
export CATALINA_OPTS="$CATALINA_OPTS -Dhttp.proxyPort=8080"
```


## Questions or comments?

Email support@connect2id.com. For bug reports and code suggestions file a 
ticket in the issue tracker.


## Acknowledgements

* Julian Krautwald, Vladislav Mladenov and Christian Mainka, security
  researchers at Horst Goertz Institute for IT-Security / Chair for Network and
  Data Security.

2021-04-14
