package com.nimbusds.openid.connect.client.vaadin;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * Log4j loggers.
 */
class Loggers {
	
	
	/**
	 * The main logger.
	 */
	static final Logger MAIN = LogManager.getLogger("MAIN");
}
