package com.nimbusds.openid.connect.client.vaadin;


import com.nimbusds.oauth2.sdk.pkce.CodeVerifier;
import com.nimbusds.openid.connect.sdk.AuthenticationRequest;


/**
 * Pending OpenID Connect authentication request and context details.
 */
public final class PendingAuthenticationRequest {
	
	
	private final AuthenticationRequest authRequest;
	
	
	private final CodeVerifier codeVerifier;
	
	
	public PendingAuthenticationRequest(final AuthenticationRequest authRequest,
					    final CodeVerifier codeVerifier) {
		
		assert authRequest != null;
		this.authRequest = authRequest;
		
		this.codeVerifier = codeVerifier;
	}
	
	
	public AuthenticationRequest getAuthenticationRequest() {
		
		return authRequest;
	}
	
	
	public CodeVerifier getCodeVerifier() {
		
		return codeVerifier;
	}
}
