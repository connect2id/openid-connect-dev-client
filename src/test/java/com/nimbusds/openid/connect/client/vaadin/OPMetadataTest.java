package com.nimbusds.openid.connect.client.vaadin;


import junit.framework.TestCase;

import com.nimbusds.openid.connect.sdk.op.OIDCProviderMetadata;


/**
 *
 */
public class OPMetadataTest extends TestCase {


	public void testExampleMetadata()
		throws Exception {

		String opMetadataString =
			"{\"acr_values_supported\": [\"\"], \"subject_types_supported\": [\"public\", \"pairwise\"], \"request_parameter_supported\": true, \"userinfo_signing_alg_values_supported\": [\"ES512\", \"PS521\", \"RS512\", \"HS512\", \"PS384\", \"RS256\", \"ES384\", \"HS256\", \"HS384\", \"PS256\", \"none\", \"ES256\", \"RS384\"], \"claims_supported\": [\"profile\", \"family_name\", \"phone_number\", \"email_verified\", \"middle_name\", \"name\", \"phone_number_verified\", \"picture\", \"locale\", \"gender\", \"zoneinfo\", \"preferred_username\", \"updated_at\", \"birthdate\", \"website\", \"given_name\", \"address\", \"nickname\", \"email\", \"sub\"], \"issuer\": \"http://openid.fred.nic.cz:8088/oidc/\", \"id_token_encryption_enc_values_supported\": [\"A128CBC-HS256\", \"A192CBC-HS384\", \"A256CBC-HS512\", \"A128GCM\", \"A192GCM\", \"A256GCM\"], \"require_request_uri_registration\": true, \"grant_types_supported\": [\"authorization_code\", \"implicit\", \"urn:ietf:params:oauth:grant-type:jwt-bearer\"], \"token_endpoint\": \"http://openid.fred.nic.cz:8088/oidc/token/\", \"request_uri_parameter_supported\": true, \"version\": \"3.0\", \"registration_endpoint\": \"http://openid.fred.nic.cz:8088/oidc/registration/\", \"response_modes_supported\": [\"query\", \"fragment\", \"form_post\"], \"jwks_uri\": \"http://openid.fred.nic.cz:8088/static/oidc/key.jwk\", \"userinfo_encryption_alg_values_supported\": [\"RSA1_5\", \"RSA-OAEP\", \"A128KW\", \"A192KW\", \"A256KW\", \"ECDH-ES\", \"ECDH-ES+A128KW\", \"ECDH-ES+A192KW\", \"ECDH-ES+A256KW\"], \"scopes_supported\": [\"profile\", \"openid\", \"offline_access\", \"phone\", \"address\", \"email\", \"openid\"], \"token_endpoint_auth_methods_supported\": [\"client_secret_post\", \"client_secret_basic\", \"client_secret_jwt\", \"private_key_jwt\"], \"userinfo_encryption_enc_values_supported\": [\"A128CBC-HS256\", \"A192CBC-HS384\", \"A256CBC-HS512\", \"A128GCM\", \"A192GCM\", \"A256GCM\"], \"id_token_signing_alg_values_supported\": [\"ES512\", \"PS521\", \"RS512\", \"HS512\", \"PS384\", \"RS256\", \"ES384\", \"HS256\", \"HS384\", \"PS256\", \"none\", \"ES256\", \"RS384\"], \"request_object_encryption_enc_values_supported\": [\"A128CBC-HS256\", \"A192CBC-HS384\", \"A256CBC-HS512\", \"A128GCM\", \"A192GCM\", \"A256GCM\"], \"claims_parameter_supported\": true, \"id_token_encryption_alg_values_supported\": [\"RSA1_5\", \"RSA-OAEP\", \"A128KW\", \"A192KW\", \"A256KW\", \"ECDH-ES\", \"ECDH-ES+A128KW\", \"ECDH-ES+A192KW\", \"ECDH-ES+A256KW\"], \"token_endpoint_auth_signing_alg_values_supported\": [\"ES512\", \"PS521\", \"RS512\", \"HS512\", \"PS384\", \"RS256\", \"ES384\", \"HS256\", \"HS384\", \"PS256\", \"ES256\", \"RS384\"], \"userinfo_endpoint\": \"http://openid.fred.nic.cz:8088/oidc/userinfo/\", \"request_object_signing_alg_values_supported\": [\"ES512\", \"PS521\", \"RS512\", \"HS512\", \"PS384\", \"RS256\", \"ES384\", \"HS256\", \"HS384\", \"PS256\", \"none\", \"ES256\", \"RS384\"], \"request_object_encryption_alg_values_supported\": [\"RSA1_5\", \"RSA-OAEP\", \"A128KW\", \"A192KW\", \"A256KW\", \"ECDH-ES\", \"ECDH-ES+A128KW\", \"ECDH-ES+A192KW\", \"ECDH-ES+A256KW\"], \"response_types_supported\": [\"code\", \"token\", \"id_token\", \"code token\", \"code id_token\", \"id_token token\", \"code token id_token\"], \"end_session_endpoint\": \"http://openid.fred.nic.cz:8088/oidc/end_session/\", \"authorization_endpoint\": \"http://openid.fred.nic.cz:8088/oidc/authorization/\", \"claim_types_supported\": [\"normal\", \"aggregated\", \"distributed\"]}";

		try {

			OIDCProviderMetadata metadata = OIDCProviderMetadata.parse(opMetadataString);
		} catch (IllegalArgumentException e) {
			// ok
		}

	}
}